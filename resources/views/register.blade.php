<!DOCTYPE html>
<html>
<head>
<title>Sign UP</title>
</head>
<body>

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form </h3>
<form method="get" action="{{url('welcome')}}">
	<label for="firstnamee"> First name:</label><br><br>
	<input type="text" name="firstname" id="firstnamee"> <br><br>

	<label for="lastname"> Last name:</label><br><br>
	<input type="text" name="lastname" id="lastname"><br><br>


	<label for="gender">Gender:</label><br><br>

	  <input type="radio" id="male" name="gender" value="male">
	  <label for="male">Male</label><br>

	  <input type="radio" id="female" name="gender" value="female">
	  <label for="female">Female</label><br>

	  <input type="radio" id="other" name="gender" value="other">
	  <label for="other">Other</label><br><br>

 <label for="nationality">Nationality:</label><br><br>
 <select name="nationality" id="nationality">
 	<option value="Indonesian"> indonesian</option>
 	<option value="singapore">Singapore</option>
 	<option value="malaysian">Malaysian</option>
 </select><br><br>

 <label for="ls">Language Spoken:</label><br><br>
	 <input type="checkbox" name="ls" id="b.indo">
	 <label for="b.indo">Bahasa Indonesia</label><br>
	 <input type="checkbox" name="ls" id="english">
	 <label for="english">English</label><br><input type="checkbox" name="ls" id="other_">
	 <label for="other_">Other<br><br>

<label for="bio">Bio:</label><br><br>
<textarea id="bio" name="bio" rows="5" cols="30"></textarea><br>

<input type="submit" name="submit" id="submit" value="Sign Up">

</form>
</body>
</html>